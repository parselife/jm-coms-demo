export default {
  name: 'JmHelloLayoutTest',
  /**
   * 布局结构
   */
  layout: [{
    // 组件名称
    name: 'JmHelloLayout',
    version: 1,
    // 合并config.xml里的属性
    data: {},
    // 子组件插槽
    slots: {
      /**
       * data-yttag
       */
      content: [ // data-yttagindex
        [ // data-yttagindex=0的位置
          {
            name: 'JmHello',
            version: 1
          },
          {
            name: 'JmHelloEcharts',
            version:1
          }
        ]
      ]
    }
  }]
}
