import { LayoutComponent } from 'yt-engine'

/**
 * 布局组件demo
 */

export default class JmHelloLayout extends LayoutComponent {
  render() {
    // data-yttagindex 默认为 0 
    return '<div data-yttag="content" data-yttagindex="0" style="width:300px;height:300px;"></div>'
  }
}
