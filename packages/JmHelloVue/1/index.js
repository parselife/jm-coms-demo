import Vue from 'vue'
import { UIComponent } from 'yt-engine'

export default class JmHelloVue extends UIComponent {
  render() {
    const { count } = this

    const vue = new Vue({
      template: `<button @click="onClick">Hello Vue! ${count}</button>`,
      methods: {
        onClick: () => {
          this.changeData({
            count: parseInt(count) + 1
          })
        }
      }
    })

    vue.$mount(undefined, true)

    return vue.$el
  }
}
