import { adaptVue } from 'yt-engine'
import { Button } from 'ant-design-vue'
import Vue from 'vue'

/**
 * @type { import('vue').ComponentOptions }
 */
const options = {
  methods: {
    test() {
      console.log('test')
    }
  },
  render() {
    const { count, $jm } = this

    const props = {
      props: {
        type: 'primary'
      },
      on: {
        click: () => {
          $jm.changeData({
            count: count+1
          })

          $jm.doEvent('onClick')
        }
      }
    }

    return <Button {...props}>
      {count}
      <span v-yt-slot={{ tag: 'default', /* 插槽, index: 0 可省略 */ }}></span>
    </Button>
  }
}

export default adaptVue(Vue.extend(options), {
  /**
   * 布局组件，这里必填，不填默认ui组件，不会显示子组件
   */
  type: 'layout',
  /**
   * 暴露给平台的属性,vue实例中直接获取即可
   */
  properties: {
    count: 'number'
  },
  /**
   * 暴露给平台的方法，vue实例中必须有该方法
   */
  methods: {
    /**
     * 这里暴露方法
     */
    test: true
  }
})
