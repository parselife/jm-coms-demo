import echarts from 'echarts'
import { UIComponent } from 'yt-engine'

export default class JmHelloEcharts extends UIComponent {
  render() {
    return '<div style="width: 100%;height: 100%;"></div>'
  }
  afterRender() {
    const { $el, style, data } = this

    Object.assign($el.style, style)

    const inst = echarts.init($el)  

    inst.setOption({
      xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      yAxis: {
        type: 'value'
      },
      series: [{
        data,
        type: 'line'
      }]
    })
  }
}
