import { UIComponent } from 'yt-engine'
import "./index.less"

export default class JmHello extends UIComponent {
  constructor(options) {
    super(options)
  }

  getComponentProperties() {
    return {
      count: 'number'
    }
  }

  getComponentMethods() {
    return {
      hello: true
    }
  }

  hello() {
    const { count } = this

    console.log(`Hello! ${count}`)
  }
  /**
   * 组件初始化
   */
  init() {}
  /**
   * 组件第一次加载到document里
   */
  didMount() {}
  /**
   * 组件属性变化时
   */
  shouldRefresh() { return true }
  beforeRender() {}
  render() {
    const { count, id } = this

    console.log('组件id', id)

    return `<button class="jm-button">Hello! ${count}</button>`
  }
  afterRender() {
    const { $el, count } = this

    $el.addEventListener('click', () => {
      this.changeData({
        count: parseInt(count) + 1
      })

      this.doEvent('onclick', count)
    })
  }
  count() {
    this.changeData({
      count: parseInt(this.count) + 1
    })
  }
  beforeDestroy() {}
  destroy() {}
}
